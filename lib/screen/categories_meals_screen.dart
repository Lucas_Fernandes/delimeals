import 'package:flutter/material.dart';
import 'package:flutter_complete_guide/model/Meal.dart';
import 'package:flutter_complete_guide/widget/listitem/meal_item.dart';

class CategoriesMealsScreen extends StatefulWidget {
  static const ROUTE_NAME = "/categories-meals";
  final List<Meal> _availableMeals;

  CategoriesMealsScreen(this._availableMeals);

  @override
  _CategoriesMealsScreenState createState() => _CategoriesMealsScreenState();
}

class _CategoriesMealsScreenState extends State<CategoriesMealsScreen> {
  Map<String, String> _category;
  List<Meal> _displayedMeals;

  @override
  void didChangeDependencies() {
    _category =
        ModalRoute.of(context).settings.arguments as Map<String, String>;

    _displayedMeals = widget._availableMeals
        .where((meal) => meal.categories.contains(_category["id"]))
        .toList();
    super.didChangeDependencies();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(_category["title"]),
      ),
      body: ListView.builder(
        itemBuilder: (ctx, index) {
          return MealItem(
            id: _displayedMeals[index].id,
            title: _displayedMeals[index].title,
            imageUrl: _displayedMeals[index].imageUrl,
            duration: _displayedMeals[index].duration,
            complexity: _displayedMeals[index].complexity,
            affordability: _displayedMeals[index].affordability,
          );
        },
        itemCount: _displayedMeals.length,
      ),
    );
  }

  void _removeMeal(String mealId) {
    setState(() {
      _displayedMeals.removeWhere((meal) => meal.id == mealId);
    });
  }
}
