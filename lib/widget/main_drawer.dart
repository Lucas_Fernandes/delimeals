import 'package:flutter/material.dart';
import 'package:flutter_complete_guide/screen/categories_meals_screen.dart';
import 'package:flutter_complete_guide/screen/filters_screen.dart';

import 'listitem/drawer_item.dart';

class MainDrawer extends StatelessWidget {

  MainDrawer();

  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: Column(
        children: [
          Container(
            height: 120,
            width: double.infinity,
            padding: EdgeInsets.all(20),
            alignment: Alignment.centerLeft,
            color: Theme.of(context).accentColor,
            child: Text(
              "Cooking Up!",
              style: TextStyle(
                fontWeight: FontWeight.w900,
                fontSize: 30,
                color: Theme.of(context).primaryColor,
              ),
            ),
          ),
          SizedBox(
            height: 20,
          ),
          DrawerItem(
            "Meals",
            Icons.restaurant,
            (CategoriesMealsScreen).toString(),
          ),
          DrawerItem(
            "Filters",
            Icons.settings,
            (FiltersScreen).toString(),
          ),
        ],
      ),
    );
  }
}
