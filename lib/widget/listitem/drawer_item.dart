import 'package:flutter/material.dart';
import 'package:flutter_complete_guide/screen/categories_meals_screen.dart';
import 'package:flutter_complete_guide/screen/filters_screen.dart';

class DrawerItem extends StatelessWidget {
  final String _title;
  final IconData _icon;
  final String _className;

  DrawerItem(this._title, this._icon, this._className);

  @override
  Widget build(BuildContext context) {
    return ListTile(
        leading: Icon(
          _icon,
          size: 26,
        ),
        title: Text(
          _title,
          style: TextStyle(
            fontFamily: "RobotoCondensed",
            fontSize: 24,
            fontWeight: FontWeight.bold,
          ),
        ),
        onTap: () => _className == (CategoriesMealsScreen).toString()
            ? _goToMeals(context)
            : _goToFilters(context),
        );
  }

  void _goToMeals(BuildContext context) {
    Navigator.of(context).pushReplacementNamed("/");
  }

  void _goToFilters(BuildContext context) {
    Navigator.of(context).pushReplacementNamed(FiltersScreen.ROUTE_NAME);
  }
}
