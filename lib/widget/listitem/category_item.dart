import 'package:flutter/material.dart';
import 'package:flutter_complete_guide/screen/categories_meals_screen.dart';

class CategoryItem extends StatelessWidget {
  final String _title;
  final Color _color;
  final String _id;

  CategoryItem(this._title, this._color, this._id);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () => _selectCategory(context),
      splashColor: Theme.of(context).primaryColor,
      borderRadius: BorderRadius.circular(15),
      child: Container(
        padding: const EdgeInsets.all(15),
        child: Text(
          _title,
          style: Theme.of(context).textTheme.headline6,
        ),
        decoration: BoxDecoration(
          gradient: LinearGradient(
            colors: [_color.withOpacity(0.7), _color],
            begin: Alignment.topCenter,
            end: Alignment.bottomRight,
          ),
          borderRadius: BorderRadius.circular(15),
        ),
      ),
    );
  }

  void _selectCategory(BuildContext ctx) {
    Navigator.of(ctx).pushNamed(
      CategoriesMealsScreen.ROUTE_NAME,
      arguments: {
        "id": _id,
        "title": _title,
      },
    );
  }
}
