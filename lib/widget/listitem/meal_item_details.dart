import 'package:flutter/material.dart';

class MealItemDetails extends StatelessWidget {
  final IconData icon;
  final String detailText;

  MealItemDetails(this.icon, this.detailText);

  @override
  Widget build(BuildContext context) {
    return Container(child: Row(
      children: [
        Icon(icon),
        SizedBox(
          width: 6,
        ),
        Text(detailText),
      ],
    ));
  }
}