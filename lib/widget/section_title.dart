import 'package:flutter/material.dart';

class SectionTitle extends StatelessWidget {
  final String _title;

  SectionTitle(this._title);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.symmetric(vertical: 10),
      child: Text(
        _title,
        style: Theme.of(context).textTheme.headline6,
      ),
    );
  }
}