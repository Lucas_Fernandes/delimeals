import 'package:flutter/material.dart';

class ListBuildContainer extends StatelessWidget {
  final Widget _child;
  final double _percentageWidth;

  ListBuildContainer(this._child, this._percentageWidth);

  @override
  Widget build(BuildContext context) {
    final deviceData = MediaQuery.of(context);
    return Container(
        decoration: BoxDecoration(
          color: Colors.white,
          border: Border.all(color: Colors.grey),
          borderRadius: BorderRadius.circular(10),
        ),
        margin: EdgeInsets.all(15),
        padding: EdgeInsets.all(10),
        height: deviceData.size.width * _percentageWidth,
        width: deviceData.size.width,
        child: _child);
  }
}
